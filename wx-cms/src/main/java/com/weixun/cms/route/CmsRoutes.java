package com.weixun.cms.route;

import com.jfinal.config.Routes;
import com.weixun.cms.controller.*;

public class CmsRoutes extends Routes{

    /**
     * CMS模块下的路由配置
     */
    @Override
    public void config()
    {
        add("/article", ArticleController.class);
        add("/link",LinkController.class);
        add("/site",SiteController.class);
        add("/channel",ChannelController.class);
        add("/ueditor",UeditorController.class);
        add("/file",FileController.class);
        add("/msg",MsgController.class);

    }
}

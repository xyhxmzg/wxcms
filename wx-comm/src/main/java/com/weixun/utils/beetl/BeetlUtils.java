package com.weixun.utils.beetl;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.FileResourceLoader;
import org.beetl.core.resource.WebAppResourceLoader;

import java.io.*;
import java.util.Map;

public class BeetlUtils {

    /**
     * 生成静态页通用方法
     * @param data  页面上需要显示的数据
     * @param templatePath 模板路径
     * @param templateName 模板名称
     * @param targetHtmlPath 生成一面路径
     */
    public static void crateHTML(Map<String,Object> data, String templatePath, String templateName, String targetHtmlPath){




        //加载模版
        Writer out = null;

        try {
            //设置要解析的模板所在的目录
//            FileResourceLoader resourceLoader = new FileResourceLoader(templatePath,"utf-8");
//            Configuration cfg = Configuration.defaultConfiguration();
//            GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
            Configuration cfg = Configuration.defaultConfiguration();
            WebAppResourceLoader resourceLoader = new WebAppResourceLoader(templatePath,"utf-8");
            GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
            gt.setSharedVars(data);
            //指定模版路径
            Template template = gt.getTemplate(templateName);
            template.fastBinding(data);

            //设置输出文件
            File htmlFile = new File(targetHtmlPath);
            //判断目录是否存在，不存在则删除并创建
            if (!htmlFile.getParentFile().exists()) {
                htmlFile.getParentFile().mkdirs();
            }
            //页面存在则删除
            if (htmlFile.exists()) {
               htmlFile.delete();
            }
            htmlFile.createNewFile();


            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(htmlFile), "utf-8"));
            template.renderTo(out);
            //合并数据模型与模板
//            template.process(data, out);

        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            try {
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}

<!DOCTYPE html>
<%--
  Created by IntelliJ IDEA.
  User: myd
  Date: 2017/6/20
  Time: 上午10:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <% String path = request.getContextPath(); %>
    <title>后台管理系统</title>
    <link rel="stylesheet" href="<%=path%>/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="<%=path%>/static/plugins/font-awesome/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="css/admin.css"/> -->
</head>
<body>
<div id="app" class="layui-form">
    <div class="container">
        <form class="layui-form" action="" id="formlist">
            <input id="channel_pk" name="channel_pk"  autocomplete="off"  class="layui-input" type="hidden"/>
            <div class="layui-form-item" style="margin: 5% auto">
                <label class="layui-form-label">菜单名称</label>
                <div class="layui-input-block">
                    <input id="channel_name" name="channel_name"  lay-verify="required"  autocomplete="off" placeholder="请输入菜单名称" class="layui-input" type="text"/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">父菜单</label>
                <div class="layui-input-block">
                    <select id="channel_parent" name="channel_parent"  lay-verify="required">
                        <option value=""></option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">栏目英文缩写</label>
                <div class="layui-input-block">
                    <input id="channel_english" name="channel_english"  autocomplete="off" placeholder="请输入栏目英文缩写" class="layui-input" type="text">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">栏目目录</label>
                <div class="layui-input-block">
                    <input id="channel_catalog" name="channel_catalog" autocomplete="off" placeholder="请输入栏目目录" class="layui-input" type="text"/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">栏目内容页</label>
                <div class="layui-input-block">
                    <input id="channel_page_name" name="channel_page_name" autocomplete="off" placeholder="请输入栏目内容页名称" class="layui-input" type="text"/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">栏目内容页模板</label>
                <div class="layui-input-block">
                    <input id="channel_page_template" name="channel_page_template" autocomplete="off" placeholder="请输入栏目内容页名称" class="layui-input" type="text"/>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">栏目索引页名称</label>
                <div class="layui-input-block">
                    <input id="channel_index_name" name="channel_index_name" autocomplete="off" placeholder="请输入栏目内容页名称" class="layui-input" type="text"/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">栏目索引页模板</label>
                <div class="layui-input-block">
                    <input id="channel_index_template" name="channel_index_template" autocomplete="off" placeholder="请输入栏目索引页模板" class="layui-input" type="text"/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">索引页内容数量</label>
                <div class="layui-input-block">
                    <input id="channel_index_count" name="channel_index_count" autocomplete="off" placeholder="索引页内容条数" class="layui-input" type="text"/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">栏目列表页名称</label>
                <div class="layui-input-block">
                    <input id="channel_list_name" name="channel_list_name" autocomplete="off" placeholder="请输入栏目列表页名称" class="layui-input" type="text"/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">栏目列表页模板</label>
                <div class="layui-input-block">
                    <input id="channel_list_template" name="channel_list_template" autocomplete="off" placeholder="请输入栏目列表页模板" class="layui-input" type="text"/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">栏目列表页内容数量</label>
                <div class="layui-input-block">
                    <input id="channel_list_count" name="channel_list_count" autocomplete="off" placeholder="栏目列表页内容数量" class="layui-input" type="text"/>
                </div>
            </div>



            <div class="layui-form-item">
                <div align="center" class="layui-input-block" style="margin: 5% auto">
                    <button class="layui-btn" align="center" onclick="save()">保存</button>
                    <button class="layui-btn" id="reset" align="center" type="reset">重置</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" src="<%=path%>/static/plugins/layui/layui.js"></script>
</body>
<%--layui--%>
<script>
    layui.use(['form','jquery'],function () {
        var $=layui.jquery;
        var form=layui.form;

        edit=function(){
            var edit_url = "<%=path%>/channel/list";
            var channel_pk = $('#channel_pk').val();
            debugger;
             if (channel_pk=="")
             {
                 return false;
             }
            $.ajax({
                url:edit_url,
                type:"post",
                data:{channel_pk:channel_pk},//form id
                datatype:"json",
                success:function(response){
                    var data = eval(response);
                    $('#channel_pk').val(data[0].channel_pk);
                    $('#channel_name').val(data[0].channel_name);
                    $('#channel_parent').val(data[0].channel_parent);
                    $('#channel_english').val(data[0].channel_english);
                    $('#channel_parent').val(data[0].channel_parent);
                    $('#channel_catalog').val(data[0].channel_catalog);
                    $('#channel_page_name').val(data[0].channel_page_name);
                    $('#channel_page_template').val(data[0].channel_page_template);
                    $('#channel_index_name').val(data[0].channel_index_name);
                    $('#channel_index_template').val(data[0].channel_index_template);
                    $('#channel_index_count').val(data[0].channel_index_count);
                    $('#channel_list_name').val(data[0].channel_list_name);
                    $('#channel_list_template').val(data[0].channel_list_template);
                    $('#channel_list_count').val(data[0].channel_list_count);
                    form.render('select');

                },
                error:function (response) {
                    parent.layer.alert(response.msg);
                }

            });
            return false;
        };

        $(function () {
           edit()
        });

        /**
         * 给下拉菜单绑定值
         * @type {string}
         */
        var url = "<%=path%>/channel/list";
        $.getJSON(url, function(response) {
            $("#channel_parent").append("<option value='0' selected=''>无</option>");  //添加一项option
            console.log(response);
            var json = eval(response); // 数组
            $.each(json, function(index, item) {
                var channel_pk = json[index].channel_pk;
                var channel_name = json[index].channel_name;
                $("#channel_parent").append("<option value=" + channel_pk + ">" + channel_name + "</option>"); // 添加一项option
            });
            form.render('select');
        });

        /**
         * 保存信息
         */
        save = function () {
            $.ajax({
                url:"<%=path%>/channel/saveOrUpdate",
                type:"post",
                async : false,//同步请求，执行成功后才会继续执行后面的代码
                data:$('#formlist').serialize(),//form id
                datatype:"json",
                success:function(response){
                    debugger;
                    parent.layer.alert(response.msg);
                },
                error:function (response) {
                    parent.layer.alert(response.msg);
                }

            });
            return false;
        }

    })
</script>
</html>
